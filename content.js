/* WebAuthn-eID for Firefox (c) 2020 kahlo@adesso */
(function content() { 'use strict';
	if (window.hasRun) { return; } else { window.hasRun = true; }
    
	function send(msg, resolve, reject) { browser.runtime.sendMessage(msg).then(function(msg) {
		console.log("resolve.msg: " + msg);
		resolve(cloneInto(msg, window)) }, function(error) { reject("" + error) })};
		
    let rpId = { origin: document.location.origin, domain: document.domain };    
    
    // WebAuthn
    exportFunction(function(options) { return new window.Promise(async function(resolve, reject) {
    	send({call: "create", rpId: rpId, options: options}, resolve, reject)})},
    	navigator.credentials, { defineAs: "create" })
    exportFunction(function(options) { return new window.Promise(async function(resolve, reject) {
    	send({call: "get", rpId: rpId, options: options}, function resolveProxy(x) {
    	    function getClientExtensionResults() {
    	    	return cloneInto({ }, window, {cloneFunctions: true});
    	    };
    	    
            exportFunction(getClientExtensionResults, x, {defineAs:'getClientExtensionResults'});       
            resolve(x);
    	}, reject)})},	
    	navigator.credentials, { defineAs: "get" });
    
    // U2F
    var u2freg = exportFunction(function(appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds) {
   		send({call: "u2freg", rpId: rpId, options: [ appId, registerRequests, registeredKeys, opt_timeoutSeconds ]},
   			function(x) { callback(x) }, function(x) { callback(x) })}, window);
    var u2fsig = exportFunction(function(appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds) {
   		send({call: "u2fsig", rpId: rpId, options: [ appId, registerRequests, registeredKeys, opt_timeoutSeconds ]},
   		   	function(x) { callback(x) }, function(x) { callback(x) })}, window);
    try { Object.defineProperty(window.wrappedJSObject, "u2f", { value:
    	cloneInto({ register: u2freg, sign: u2fsig }, window, {cloneFunctions: true}) }); } catch(e) {};
})();
